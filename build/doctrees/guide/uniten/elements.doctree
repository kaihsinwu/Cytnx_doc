���G      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Get/set UniTensor element�h]�h	�Text����Get/set UniTensor element�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�B/home/kaihsinwu/Dropbox/Cytnx_doc/source/guide/uniten/elements.rst�hKubh	�	paragraph���)��}�(hX9  In this section, we discuss how to get an element directly from a UniTensor and how to set an element. Generally, elements can be accessed by first getting the corresponding block, and then accessing the correct element from that block. However, it is also possible to directly access an element from a UniTensor.�h]�hX9  In this section, we discuss how to get an element directly from a UniTensor and how to set an element. Generally, elements can be accessed by first getting the corresponding block, and then accessing the correct element from that block. However, it is also possible to directly access an element from a UniTensor.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(h��To get an element, one can call **UniTensor.at()**. It returns a *proxy* which contains a reference to the element. Furthermore, the proxy can be used to check whether an element corresponds to a valid block in a UniTensor with symmetries.�h]�(h� To get an element, one can call �����}�(hh=hhhNhNubh	�strong���)��}�(h�**UniTensor.at()**�h]�h�UniTensor.at()�����}�(hhGhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hEhh=ubh�. It returns a �����}�(hh=hhhNhNubh	�emphasis���)��}�(h�*proxy*�h]�h�proxy�����}�(hh[hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh=ubh�� which contains a reference to the element. Furthermore, the proxy can be used to check whether an element corresponds to a valid block in a UniTensor with symmetries.�����}�(hh=hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�UniTensor without symmetries�h]�h�UniTensor without symmetries�����}�(hhvhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhshhhh,hK
ubh.)��}�(h�WAccessing an element in a UniTensor without symmetries is straightforward by using *at*�h]�(h�SAccessing an element in a UniTensor without symmetries is straightforward by using �����}�(hh�hhhNhNubhZ)��}�(h�*at*�h]�h�at�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhshhubh	�bullet_list���)��}�(hhh]�h	�	list_item���)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhh�hhubah}�(h!]�h#]�h%]�h']�h)]��bullet��*�uh+h�hh,hKhhshhubh	�literal_block���)��}�(h�KT = cytnx.UniTensor(cytnx.arange(9).reshape(3,3))
print(T.at([0,2]).value)
�h]�h�KT = cytnx.UniTensor(cytnx.arange(9).reshape(3,3))
print(T.at([0,2]).value)
�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��source��W/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_uniten_elements_at_get.py��	xml:space��preserve��force���language��python��linenos���highlight_args�}��linenostart�Ksuh+h�hh,hKhhshhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhh�hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKhhshhubh�)��}�(h�Pauto T = cytnx::UniTensor(cytnx::arange(9).reshape(3, 3));
print(T.at({0, 2}));
�h]�h�Pauto T = cytnx::UniTensor(cytnx::arange(9).reshape(3, 3));
print(T.at({0, 2}));
�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]��source��[/home/kaihsinwu/Dropbox/Cytnx_doc/code/cplusplus/doc_codes/guide_uniten_elements_at_get.cpp�h�h�hۉh܌c++�hވh�}�h�Ksuh+h�hh,hKhhshhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhshhubh�)��}�(h�2.0
�h]�h�2.0
�����}�hj#  sbah}�(h!]�h#]�h%]�h']�h)]��source��V/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_uniten_elements_at_get.out�h�h�hۉh܌text�h�}�h�Ksuh+h�hh,hKhhshhubh	�note���)��}�(h�2Note that in Python, adding *.value* is necessary!�h]�h.)��}�(hj9  h]�(h�Note that in Python, adding �����}�(hj;  hhhNhNubhZ)��}�(h�*.value*�h]�h�.value�����}�(hjB  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj;  ubh� is necessary!�����}�(hj;  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK!hj7  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j5  hh,hKhhshhubh.)��}�(h�eThe proxy returned by **at** also serves as reference, so we can directly assign or modify the value:�h]�(h�The proxy returned by �����}�(hj`  hhhNhNubhF)��}�(h�**at**�h]�h�at�����}�(hjh  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hEhj`  ubh�I also serves as reference, so we can directly assign or modify the value:�����}�(hj`  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK$hhshhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK&hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK&hj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hK&hhshhubh�)��}�(h�zT = cytnx.UniTensor(cytnx.arange(9).reshape(3,3))
print(T.at([0,2]).value)
T.at([0,2]).value = 7
print(T.at([0,2]).value)
�h]�h�zT = cytnx.UniTensor(cytnx.arange(9).reshape(3,3))
print(T.at([0,2]).value)
T.at([0,2]).value = 7
print(T.at([0,2]).value)
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��W/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_uniten_elements_at_set.py�h�h�hۉh܌python�hވh�}�h�Ksuh+h�hh,hK(hhshhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK,hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK,hj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hK,hhshhubh�)��}�(h�wauto T = cytnx::UniTensor(cytnx::arange(9).reshape(3, 3));
print(T.at({0, 2}));
T.at({0, 2}) = 7;
print(T.at({0, 2}));
�h]�h�wauto T = cytnx::UniTensor(cytnx::arange(9).reshape(3, 3));
print(T.at({0, 2}));
T.at({0, 2}) = 7;
print(T.at({0, 2}));
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��[/home/kaihsinwu/Dropbox/Cytnx_doc/code/cplusplus/doc_codes/guide_uniten_elements_at_set.cpp�h�h�hۉh܌c++�hވh�}�h�Ksuh+h�hh,hK.hhshhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK2hhshhubh�)��}�(h�2.0
7.0
�h]�h�2.0
7.0
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��V/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_uniten_elements_at_set.out�h�h�hۉh܌text�h�}�h�Ksuh+h�hh,hK4hhshhubeh}�(h!]��unitensor-without-symmetries�ah#]�h%]��unitensor without symmetries�ah']�h)]�uh+h
hhhhhh,hK
ubh)��}�(hhh]�(h)��}�(h�UniTensor with symmetries�h]�h�UniTensor with symmetries�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hK9ubh.)��}�(h��When a UniTensor has block structure, not all possible elements correspond to a valid block. Invalid elements do not fulfill the symmetries. Therefore, these invalid elements should not be accessed.�h]�h��When a UniTensor has block structure, not all possible elements correspond to a valid block. Invalid elements do not fulfill the symmetries. Therefore, these invalid elements should not be accessed.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK;hj  hhubh.)��}�(h�rIn such cases, one can still use *at* and receive a proxy. The proxy can be used to check if the element is valid.�h]�(h�!In such cases, one can still use �����}�(hj-  hhhNhNubhZ)��}�(h�*at*�h]�h�at�����}�(hj5  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj-  ubh�M and receive a proxy. The proxy can be used to check if the element is valid.�����}�(hj-  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK=hj  hhubh.)��}�(h�RLet's consider the same example of a symmetric tensor as in the previous sections:�h]�h�TLet’s consider the same example of a symmetric tensor as in the previous sections:�����}�(hjM  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK?hj  hhubh	�image���)��}�(h�@.. image:: image/u1_tdex.png
    :width: 500
    :align: center
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��500��align��center��uri��guide/uniten/image/u1_tdex.png��
candidates�}�h�jl  s�original_uri��image/u1_tdex.png�uh+j[  hh,hKAhj  hhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hjx  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKEhjt  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKEhjq  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKEhj  hhubh�)��}�(hX�  bond_d = cytnx.Bond(
    cytnx.BD_IN, 
    [cytnx.Qs(1)>>1, cytnx.Qs(-1)>>1],
    [cytnx.Symmetry.U1()])

bond_e = cytnx.Bond(
    cytnx.BD_IN, 
    [cytnx.Qs(1)>>1, cytnx.Qs(-1)>>1],
    [cytnx.Symmetry.U1()])

bond_f = cytnx.Bond(
    cytnx.BD_OUT,
    [cytnx.Qs(2)>>1, cytnx.Qs(0)>>2, 
     cytnx.Qs(-2)>>1],
     [cytnx.Symmetry.U1()])

Tsymm = cytnx.UniTensor([bond_d, bond_e, bond_f], 
                        name="symm. tensor", 
                        labels=["d","e","f"])
�h]�hX�  bond_d = cytnx.Bond(
    cytnx.BD_IN, 
    [cytnx.Qs(1)>>1, cytnx.Qs(-1)>>1],
    [cytnx.Symmetry.U1()])

bond_e = cytnx.Bond(
    cytnx.BD_IN, 
    [cytnx.Qs(1)>>1, cytnx.Qs(-1)>>1],
    [cytnx.Symmetry.U1()])

bond_f = cytnx.Bond(
    cytnx.BD_OUT,
    [cytnx.Qs(2)>>1, cytnx.Qs(0)>>2, 
     cytnx.Qs(-2)>>1],
     [cytnx.Symmetry.U1()])

Tsymm = cytnx.UniTensor([bond_d, bond_e, bond_f], 
                        name="symm. tensor", 
                        labels=["d","e","f"])
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��Y/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_uniten_elements_init_sym.py�h�h�hۉh܌python�hވh�}�h�Ksuh+h�hh,hKGhj  hhubh.)��}�(h�YAn existing element (here: at [0,0,0]) can be accessed as in the case without symmetries:�h]�h�YAn existing element (here: at [0,0,0]) can be accessed as in the case without symmetries:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKKhj  hhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKMhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKMhj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKMhj  hhubh�)��}�(h�print(Tsymm.at([0,0,0]).value)
�h]�h�print(Tsymm.at([0,0,0]).value)
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��X/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_uniten_elements_at_qidx.py�h�h�hۉh܌python�hވh�}�h�Ksuh+h�hh,hKOhj  hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKShj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKShj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKShj  hhubh�)��}�(h�print(Tsymm.at({0,0,0}));�h]�h�print(Tsymm.at({0,0,0}));�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�hވhۉh܌c++�h�}�uh+h�hh,hKUhj  hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKZhj  hhubh�)��}�(h�0.0
�h]�h�0.0
�����}�hj$  sbah}�(h!]�h#]�h%]�h']�h)]��source��W/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_uniten_elements_at_qidx.out�h�h�hۉh܌text�h�}�h�Ksuh+h�hh,hK\hj  hhubh.)��}�(h�vIf we try to access an element that does not correspond to a valid block (for example at [0,0,1]), an error is thrown:�h]�h�vIf we try to access an element that does not correspond to a valid block (for example at [0,0,1]), an error is thrown:�����}�(hj6  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK`hj  hhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hjK  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKbhjG  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKbhjD  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKbhj  hhubh�)��}�(h�print(Tsymm.at([0,0,1]).value)
�h]�h�print(Tsymm.at([0,0,1]).value)
�����}�hje  sbah}�(h!]�h#]�h%]�h']�h)]��source��]/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_uniten_elements_at_non_exist.py�h�h�hۉh܌python�hވh�}�h�Ksuh+h�hh,hKdhj  hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj~  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhjz  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhhjw  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKhhj  hhubh�)��}�(h�print(Tsymm.at({0,0,1}));�h]�h�print(Tsymm.at({0,0,1}));�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�hވhۉh܌c++�h�}�uh+h�hh,hKjhj  hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKohj  hhubh�)��}�(h��[ERROR] trying access an element that is not exists!, using T.if_exists = sth or checking with T.exists() to verify before access element!
�h]�h��[ERROR] trying access an element that is not exists!, using T.if_exists = sth or checking with T.exists() to verify before access element!
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��\/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_uniten_elements_at_non_exist.out�h�h�hۉh܌text�h�}�h�Ksuh+h�hh,hKqhj  hhubh.)��}�(h��To avoid this error, we can check if the element is valid before accessing it. The proxy provides the method **exists()** for this purpose. For example, if we want to assign the value 8 to all valid elements with indices of the form [0,0,i], we can use:�h]�(h�mTo avoid this error, we can check if the element is valid before accessing it. The proxy provides the method �����}�(hj�  hhhNhNubhF)��}�(h�**exists()**�h]�h�exists()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hEhj�  ubh�� for this purpose. For example, if we want to assign the value 8 to all valid elements with indices of the form [0,0,i], we can use:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKthj  hhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKvhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKvhj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKvhj  hhubh�)��}�(h�Yfor i in [0,1]:
    tmp = Tsymm.at([0,0,i])
    if(tmp.exists()):
        tmp.value = 8.
�h]�h�Yfor i in [0,1]:
    tmp = Tsymm.at([0,0,i])
    if(tmp.exists()):
        tmp.value = 8.
�����}�hj	  sbah}�(h!]�h#]�h%]�h']�h)]��source��W/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_uniten_elements_exists.py�h�h�hۉh܌python�hވh�}�h�Ksuh+h�hh,hKxhj  hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj"  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK|hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK|hj  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hK|hj  hhubh�)��}�(h�afor(auto i=0;i<2;i++){
    auto tmp = Tsymm.at({0,0,i});
    if(tmp.exists()):
        tmp = 8;
}�h]�h�afor(auto i=0;i<2;i++){
    auto tmp = Tsymm.at({0,0,i});
    if(tmp.exists()):
        tmp = 8;
}�����}�hj<  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�hވhۉh܌c++�h�}�uh+h�hh,hK~hj  hhubh.)��}�(h�aThis will set the element at [0,0,0] to 8 while ignoring the [0,0,1] element that does not exist.�h]�h�aThis will set the element at [0,0,0] to 8 while ignoring the [0,0,1] element that does not exist.�����}�(hjL  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj  hhubh	�compound���)��}�(hhh]�h �toctree���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�h�guide/uniten/elements��entries�]��includefiles�]��maxdepth�J�����caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh+j_  hh,hK�hj\  ubah}�(h!]�h#]��toctree-wrapper�ah%]�h']�h)]�uh+jZ  hj  hhhh,hK�ubeh}�(h!]��unitensor-with-symmetries�ah#]�h%]��unitensor with symmetries�ah']�h)]�uh+h
hhhhhh,hK9ubeh}�(h!]��get-set-unitensor-element�ah#]�h%]��get/set unitensor element�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�root_prefix��/��source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks���sectnum_xform���strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform���sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j  j  j�  j�  u�	nametypes�}�(j�  �j  �j�  �uh!}�(j�  hj  hsj�  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.