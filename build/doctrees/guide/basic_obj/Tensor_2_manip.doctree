��y      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Manipulating Tensors�h]�h	�Text����Manipulating Tensors�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�K/home/kaihsinwu/Dropbox/Cytnx_doc/source/guide/basic_obj/Tensor_2_manip.rst�hKubh	�	paragraph���)��}�(h�WNext, let's look at the operations that are commonly used to manipulate Tensor objects.�h]�h�YNext, let’s look at the operations that are commonly used to manipulate Tensor objects.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�reshape�h]�h�reshape�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh=hhhh,hKubh.)��}�(h��Suppose we want to create a rank-3 Tensor with shape=(2,3,4), starting with a rank-1 Tensor with shape=(24) initialized using **arange()**.�h]�(h�~Suppose we want to create a rank-3 Tensor with shape=(2,3,4), starting with a rank-1 Tensor with shape=(24) initialized using �����}�(hhNhhhNhNubh	�strong���)��}�(h�**arange()**�h]�h�arange()�����}�(hhXhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhhNubh�.�����}�(hhNhhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh.)��}�(h�"This operation is called *reshape*�h]�(h�This operation is called �����}�(hhphhhNhNubh	�emphasis���)��}�(h�	*reshape*�h]�h�reshape�����}�(hhzhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhhpubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK	hh=hhubh.)��}�(h�6We can use the **Tensor.reshape** function to do this.�h]�(h�We can use the �����}�(hh�hhhNhNubhW)��}�(h�**Tensor.reshape**�h]�h�Tensor.reshape�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhh�ubh� function to do this.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh	�bullet_list���)��}�(hhh]�h	�	list_item���)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhh�hhubah}�(h!]�h#]�h%]�h']�h)]��bullet��*�uh+h�hh,hKhh=hhubh	�literal_block���)��}�(h�<A = cytnx.arange(24)
B = A.reshape(2,3,4)
print(A)
print(B)
�h]�h�<A = cytnx.arange(24)
B = A.reshape(2,3,4)
print(A)
print(B)
�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��source��a/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_basic_obj_Tensor_2_manip_reshape.py��	xml:space��preserve��force���language��python��linenos���highlight_args�}��linenostart�Ksuh+h�hh,hKhh=hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhh�hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKhh=hhubh�)��}�(h�_auto A = cytnx::arange(24);
auto B = A.reshape(2, 3, 4);
cout << A << endl;
cout << B << endl;
�h]�h�_auto A = cytnx::arange(24);
auto B = A.reshape(2, 3, 4);
cout << A << endl;
cout << B << endl;
�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]��source��e/home/kaihsinwu/Dropbox/Cytnx_doc/code/cplusplus/doc_codes/guide_basic_obj_Tensor_2_manip_reshape.cpp�h�h�h�h�c++�h�h�}�h�Ksuh+h�hh,hKhh=hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj#  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh�)��}�(hX�  
Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (24)
[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]




Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



�h]�hX�  
Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (24)
[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]




Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



�����}�hj1  sbah}�(h!]�h#]�h%]�h']�h)]��source��`/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_basic_obj_Tensor_2_manip_reshape.out�h�h�h�h�text�h�}�h�Ksuh+h�hh,hKhh=hhubh.)��}�(h��Notice that calling **reshape()** returns a new object *B*, so the original object *A*'s shape is not changed after calling reshape.�h]�(h�Notice that calling �����}�(hjC  hhhNhNubhW)��}�(h�**reshape()**�h]�h�	reshape()�����}�(hjK  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhjC  ubh� returns a new object �����}�(hjC  hhhNhNubhy)��}�(h�*B*�h]�h�B�����}�(hj]  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhjC  ubh�, so the original object �����}�(hjC  hhhNhNubhy)��}�(h�*A*�h]�h�A�����}�(hjo  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhjC  ubh�0’s shape is not changed after calling reshape.�����}�(hjC  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh.)��}�(h��The function **Tensor.reshape_** (with a underscore) performs a reshape as well, but instead of returning a new reshaped object, it performs an inplace reshape of the instance that calls the function. For example:�h]�(h�The function �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.reshape_**�h]�h�Tensor.reshape_�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh�� (with a underscore) performs a reshape as well, but instead of returning a new reshaped object, it performs an inplace reshape of the instance that calls the function. For example:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK hh=hhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK"hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK"hj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hK"hh=hhubh�)��}�(h�9A = cytnx.arange(24)
print(A)
A.reshape_(2,3,4)
print(A)
�h]�h�9A = cytnx.arange(24)
print(A)
A.reshape_(2,3,4)
print(A)
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��b/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_basic_obj_Tensor_2_manip_reshape_.py�h�h�h�h�python�h�h�}�h�Ksuh+h�hh,hK$hh=hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK(hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK(hj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hK(hh=hhubh�)��}�(h�Wauto A = cytnx::arange(24);
cout << A << endl;
A.reshape_(2, 3, 4);
cout << A << endl;
�h]�h�Wauto A = cytnx::arange(24);
cout << A << endl;
A.reshape_(2, 3, 4);
cout << A << endl;
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��f/home/kaihsinwu/Dropbox/Cytnx_doc/code/cplusplus/doc_codes/guide_basic_obj_Tensor_2_manip_reshape_.cpp�h�h�h�h�c++�h�h�}�h�Ksuh+h�hh,hK*hh=hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK.hh=hhubh�)��}�(hX�  
Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (24)
[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]




Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



�h]�hX�  
Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (24)
[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]




Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]��source��a/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_basic_obj_Tensor_2_manip_reshape_.out�h�h�h�h�text�h�}�h�Ksuh+h�hh,hK0hh=hhubh.)��}�(h�SThus, we see that using the underscore version modifies the original Tensor itself.�h]�h�SThus, we see that using the underscore version modifies the original Tensor itself.�����}�(hj-  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK3hh=hhubh	�note���)��}�(h��In general, all the funcions in Cytnx that end with an underscore _ are either inplace functions that modify the instance that calls it, or return a reference of some class member.�h]�h.)��}�(hj?  h]�h��In general, all the funcions in Cytnx that end with an underscore _ are either inplace functions that modify the instance that calls it, or return a reference of some class member.�����}�(hjA  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK8hj=  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j;  hh,hK6hh=hhubh	�hint���)��}�(h�<You can use **Tensor.shape()** to get the shape of a Tensor.�h]�h.)��}�(hjX  h]�(h�You can use �����}�(hjZ  hhhNhNubhW)��}�(h�**Tensor.shape()**�h]�h�Tensor.shape()�����}�(hja  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhjZ  ubh� to get the shape of a Tensor.�����}�(hjZ  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK<hjV  ubah}�(h!]�h#]�h%]�h']�h)]�uh+jT  hh,hK:hh=hhubeh}�(h!]��reshape�ah#]�h%]��reshape�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�permute�h]�h�permute�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK?ubh.)��}�(h��Let's consider the same rank-3 Tensor with shape=(2,3,4) as an example. This time we want to permute the order of the Tensor indices according to (0,1,2)->(1,2,0)�h]�h��Let’s consider the same rank-3 Tensor with shape=(2,3,4) as an example. This time we want to permute the order of the Tensor indices according to (0,1,2)->(1,2,0)�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK@hj�  hhubh.)��}�(h�,This can be achieved with **Tensor.permute**�h]�(h�This can be achieved with �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.permute**�h]�h�Tensor.permute�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKBhj�  hhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKDhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKDhj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKDhj�  hhubh�)��}�(h�KA = cytnx.arange(24).reshape(2,3,4)
B = A.permute(1,2,0)
print(A)
print(B)
�h]�h�KA = cytnx.arange(24).reshape(2,3,4)
B = A.permute(1,2,0)
print(A)
print(B)
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��a/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_basic_obj_Tensor_2_manip_permute.py�h�h�h�h�python�h�h�}�h�Ksuh+h�hh,hKFhj�  hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKJhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKJhj�  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKJhj�  hhubh�)��}�(h�pauto A = cytnx::arange(24).reshape(2, 3, 4);
auto B = A.permute(1, 2, 0);
cout << A << endl;
cout << B << endl;
�h]�h�pauto A = cytnx::arange(24).reshape(2, 3, 4);
auto B = A.permute(1, 2, 0);
cout << A << endl;
cout << B << endl;
�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]��source��e/home/kaihsinwu/Dropbox/Cytnx_doc/code/cplusplus/doc_codes/guide_basic_obj_Tensor_2_manip_permute.cpp�h�h�h�h�c++�h�h�}�h�Ksuh+h�hh,hKLhj�  hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj(  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKPhj�  hhubh�)��}�(hX=  
Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]




Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (3,4,2)
[[[0.00000e+00 1.20000e+01 ]
  [1.00000e+00 1.30000e+01 ]
  [2.00000e+00 1.40000e+01 ]
  [3.00000e+00 1.50000e+01 ]]
 [[4.00000e+00 1.60000e+01 ]
  [5.00000e+00 1.70000e+01 ]
  [6.00000e+00 1.80000e+01 ]
  [7.00000e+00 1.90000e+01 ]]
 [[8.00000e+00 2.00000e+01 ]
  [9.00000e+00 2.10000e+01 ]
  [1.00000e+01 2.20000e+01 ]
  [1.10000e+01 2.30000e+01 ]]]



�h]�hX=  
Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]




Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (3,4,2)
[[[0.00000e+00 1.20000e+01 ]
  [1.00000e+00 1.30000e+01 ]
  [2.00000e+00 1.40000e+01 ]
  [3.00000e+00 1.50000e+01 ]]
 [[4.00000e+00 1.60000e+01 ]
  [5.00000e+00 1.70000e+01 ]
  [6.00000e+00 1.80000e+01 ]
  [7.00000e+00 1.90000e+01 ]]
 [[8.00000e+00 2.00000e+01 ]
  [9.00000e+00 2.10000e+01 ]
  [1.00000e+01 2.20000e+01 ]
  [1.10000e+01 2.30000e+01 ]]]



�����}�hj6  sbah}�(h!]�h#]�h%]�h']�h)]��source��`/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_basic_obj_Tensor_2_manip_permute.out�h�h�h�h�text�h�}�h�Ksuh+h�hh,hKRhj�  hhubj<  )��}�(h��Just like before, there is an equivalent **Tensor.permute_**, which ends with an underscore, that performs an inplace permute on the instance that calls it.�h]�h.)��}�(hjJ  h]�(h�)Just like before, there is an equivalent �����}�(hjL  hhhNhNubhW)��}�(h�**Tensor.permute_**�h]�h�Tensor.permute_�����}�(hjS  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhjL  ubh�`, which ends with an underscore, that performs an inplace permute on the instance that calls it.�����}�(hjL  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKWhjH  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j;  hh,hKUhj�  hhubh.)��}�(hX$  In Cytnx, the permute operation does not move the elements in the memory immediately. Only the meta-data that is seen by the user is changed.
This can avoid the redundant moving of elements. Note that this approach is also used in :numpy-arr:`numpy.array <>` and :torch-tn:`torch.tensor <>` .�h]�(h��In Cytnx, the permute operation does not move the elements in the memory immediately. Only the meta-data that is seen by the user is changed.
This can avoid the redundant moving of elements. Note that this approach is also used in �����}�(hjq  hhhNhNubh	�	reference���)��}�(h�numpy.array�h]�h�numpy.array�����}�(hj{  hhhNhNubah}�(h!]�h#]��extlink-numpy-arr�ah%]�h']�h)]��internal���refuri��?https://numpy.org/doc/1.18/reference/generated/numpy.array.html�uh+jy  hjq  ubh� and �����}�(hjq  hhhNhNubjz  )��}�(h�torch.tensor�h]�h�torch.tensor�����}�(hj�  hhhNhNubah}�(h!]�h#]��extlink-torch-tn�ah%]�h']�h)]��internal���refuri��,https://pytorch.org/docs/stable/tensors.html�uh+jy  hjq  ubh� .�����}�(hjq  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKZhj�  hhubh.)��}�(hX'  After the permute, the meta-data does not correspond to the memory order anymore. If the meta-data is distached that way from the real memory layout, we call the Tensor in this status *non-contiguous*. We can use **Tensor.is_contiguous()** to check if the current Tensor is in contiguous status.�h]�(h��After the permute, the meta-data does not correspond to the memory order anymore. If the meta-data is distached that way from the real memory layout, we call the Tensor in this status �����}�(hj�  hhhNhNubhy)��}�(h�*non-contiguous*�h]�h�non-contiguous�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj�  ubh�. We can use �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.is_contiguous()**�h]�h�Tensor.is_contiguous()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh�8 to check if the current Tensor is in contiguous status.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK]hj�  hhubh.)��}�(hX  You can force the Tensor to become contiguous by calling **Tensor.contiguous()** or **Tensor.contiguous_()**. The memory is then rearranged according to the shape of the Tensor. Generally you do not have to worry about the contiguous status, as Cytnx automatically handles it for you.�h]�(h�9You can force the Tensor to become contiguous by calling �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous()**�h]�h�Tensor.contiguous()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh� or �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous_()**�h]�h�Tensor.contiguous_()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh��. The memory is then rearranged according to the shape of the Tensor. Generally you do not have to worry about the contiguous status, as Cytnx automatically handles it for you.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK_hj�  hhubh�)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKbhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKbhj  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKbhj�  hhubh�)��}�(h��A = cytnx.arange(24).reshape(2,3,4)
print(A.is_contiguous())
print(A)

A.permute_(1,0,2)
print(A.is_contiguous())
print(A)

A.contiguous_()
print(A.is_contiguous())
�h]�h��A = cytnx.arange(24).reshape(2,3,4)
print(A.is_contiguous())
print(A)

A.permute_(1,0,2)
print(A.is_contiguous())
print(A)

A.contiguous_()
print(A.is_contiguous())
�����}�hj2  sbah}�(h!]�h#]�h%]�h']�h)]��source��d/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/doc_codes/guide_basic_obj_Tensor_2_manip_contiguous.py�h�h�h�h�python�h�h�}�h�Ksuh+h�hh,hKdhj�  hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hjK  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhjG  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhhjD  hhubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKhhj�  hhubh�)��}�(h��auto A = cytnx::arange(24).reshape(2, 3, 4);
cout << A.is_contiguous() << endl;
cout << A << endl;

A.permute_(1, 0, 2);
cout << A.is_contiguous() << endl;
cout << A << endl;

A.contiguous_();
cout << A.is_contiguous() << endl;
�h]�h��auto A = cytnx::arange(24).reshape(2, 3, 4);
cout << A.is_contiguous() << endl;
cout << A << endl;

A.permute_(1, 0, 2);
cout << A.is_contiguous() << endl;
cout << A << endl;

A.contiguous_();
cout << A.is_contiguous() << endl;
�����}�hje  sbah}�(h!]�h#]�h%]�h']�h)]��source��h/home/kaihsinwu/Dropbox/Cytnx_doc/code/cplusplus/doc_codes/guide_basic_obj_Tensor_2_manip_contiguous.cpp�h�h�h�h�c++�h�h�}�h�Ksuh+h�hh,hKjhj�  hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hjw  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKnhj�  hhubh�)��}�(hX/  True

Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



False

Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (3,2,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]]
 [[4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]]
 [[8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



True
�h]�hX/  True

Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]]
 [[1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



False

Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (3,2,4)
[[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 ]
  [1.20000e+01 1.30000e+01 1.40000e+01 1.50000e+01 ]]
 [[4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]
  [1.60000e+01 1.70000e+01 1.80000e+01 1.90000e+01 ]]
 [[8.00000e+00 9.00000e+00 1.00000e+01 1.10000e+01 ]
  [2.00000e+01 2.10000e+01 2.20000e+01 2.30000e+01 ]]]



True
�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��source��c/home/kaihsinwu/Dropbox/Cytnx_doc/code/python/outputs/guide_basic_obj_Tensor_2_manip_contiguous.out�h�h�h�h�text�h�}�h�Ksuh+h�hh,hKphj�  hhubh	�tip���)��}�(hX�  1. Generally, you don't have to worry about contiguous issues. You can access the elements and call linalg just like this contiguous/non-contiguous property does not exist.

2. In cases where a function does require the user to manually force the Tensor to be contiguous, a warning will be prompted, and you can simply add a **Tensor.contiguous()** or **Tensor.contiguous_()** before the function call.

3. Making a Tensor contiguous involves copying the elements in memory and can slow down the algorithm. Unnecessary calls of **Tensor.contiguous()** or **Tensor.contiguous_()** should therefore be avoided.

4. See :ref:`Contiguous` for more details about the contiguous status.�h]�h	�enumerated_list���)��}�(hhh]�(h�)��}�(h��Generally, you don't have to worry about contiguous issues. You can access the elements and call linalg just like this contiguous/non-contiguous property does not exist.
�h]�h.)��}�(h��Generally, you don't have to worry about contiguous issues. You can access the elements and call linalg just like this contiguous/non-contiguous property does not exist.�h]�h��Generally, you don’t have to worry about contiguous issues. You can access the elements and call linalg just like this contiguous/non-contiguous property does not exist.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKuhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKuhj�  ubh�)��}�(h��In cases where a function does require the user to manually force the Tensor to be contiguous, a warning will be prompted, and you can simply add a **Tensor.contiguous()** or **Tensor.contiguous_()** before the function call.
�h]�h.)��}�(h��In cases where a function does require the user to manually force the Tensor to be contiguous, a warning will be prompted, and you can simply add a **Tensor.contiguous()** or **Tensor.contiguous_()** before the function call.�h]�(h��In cases where a function does require the user to manually force the Tensor to be contiguous, a warning will be prompted, and you can simply add a �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous()**�h]�h�Tensor.contiguous()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh� or �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous_()**�h]�h�Tensor.contiguous_()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh� before the function call.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKwhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKwhj�  ubh�)��}�(h��Making a Tensor contiguous involves copying the elements in memory and can slow down the algorithm. Unnecessary calls of **Tensor.contiguous()** or **Tensor.contiguous_()** should therefore be avoided.
�h]�h.)��}�(h��Making a Tensor contiguous involves copying the elements in memory and can slow down the algorithm. Unnecessary calls of **Tensor.contiguous()** or **Tensor.contiguous_()** should therefore be avoided.�h]�(h�yMaking a Tensor contiguous involves copying the elements in memory and can slow down the algorithm. Unnecessary calls of �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous()**�h]�h�Tensor.contiguous()�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh� or �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous_()**�h]�h�Tensor.contiguous_()�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh� should therefore be avoided.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKyhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKyhj�  ubh�)��}�(h�CSee :ref:`Contiguous` for more details about the contiguous status.�h]�h.)��}�(hj4  h]�(h�See �����}�(hj6  hhhNhNubh �pending_xref���)��}�(h�:ref:`Contiguous`�h]�h	�inline���)��}�(hjA  h]�h�
Contiguous�����}�(hjE  hhhNhNubah}�(h!]�h#]�(�xref��std��std-ref�eh%]�h']�h)]�uh+jC  hj?  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��guide/basic_obj/Tensor_2_manip��	refdomain�jP  �reftype��ref��refexplicit���refwarn���	reftarget��
contiguous�uh+j=  hh,hK{hj6  ubh�. for more details about the contiguous status.�����}�(hj6  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK{hj2  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK{hj�  ubeh}�(h!]�h#]�h%]�h']�h)]��enumtype��arabic��prefix�h�suffix��.�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hh,hKshj�  hhubj<  )��}�(hXG  As mentioned before, **Tensor.contiguous_()** (with underscore) makes the current instance contiguous, while **Tensor.contiguous()** returns a new object with contiguous status.
In the case that the current instance is already in it's contiguous status, calling contiguous will return itself, and no new object will be created.�h]�h.)��}�(hXG  As mentioned before, **Tensor.contiguous_()** (with underscore) makes the current instance contiguous, while **Tensor.contiguous()** returns a new object with contiguous status.
In the case that the current instance is already in it's contiguous status, calling contiguous will return itself, and no new object will be created.�h]�(h�As mentioned before, �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous_()**�h]�h�Tensor.contiguous_()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh�@ (with underscore) makes the current instance contiguous, while �����}�(hj�  hhhNhNubhW)��}�(h�**Tensor.contiguous()**�h]�h�Tensor.contiguous()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hVhj�  ubh�� returns a new object with contiguous status.
In the case that the current instance is already in it’s contiguous status, calling contiguous will return itself, and no new object will be created.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j;  hh,hK~hj�  hhubh	�compound���)��}�(hhh]�h �toctree���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�hj\  �entries�]��includefiles�]��maxdepth�J�����caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh+j�  hh,hK�hj�  ubah}�(h!]�h#]��toctree-wrapper�ah%]�h']�h)]�uh+j�  hj�  hhhh,hK�ubeh}�(h!]��permute�ah#]�h%]��permute�ah']�h)]�uh+h
hhhhhh,hK?ubeh}�(h!]��manipulating-tensors�ah#]�h%]��manipulating tensors�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�root_prefix��/��source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks���sectnum_xform���strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform���sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  �j�  �uh!}�(j�  hj�  h=j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.