# Cytnx_doc
The documentation of the Cytnx tensor network library


# Compilation instructions
Use Rye (or other python package management system that support pyproject.toml)

```bash
   $rye sync
   $rye run make_doc
```

If you want to run the unit test in the documentation, see [here](./tests/README.md).
